import React from 'react'
import axios from 'axios'
import { APIKey, APIToken } from '../Api'
import { CloseIcon } from '@chakra-ui/icons'
const DeleteCheckItems = ({ checkListId, checkItemId, onCheckItemDeleted }) => {

  function deleteCheckItems() {
    axios.delete(`https://api.trello.com/1/checklists/${checkListId}/checkItems/${checkItemId}?key=${APIKey}&token=${APIToken}`)
      .then((response) => {
        onCheckItemDeleted(checkItemId);
      })
      .catch((error) => console.log(error))
  }
  return (
    <>
      <CloseIcon onClick={deleteCheckItems} mr={9} sx={{ fontSize: '12px' }} mt={4} cursor='pointer' />
    </>
  )
}

export default DeleteCheckItems