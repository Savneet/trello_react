import React from 'react'
import InputModal from '../InputModal'
import axios from 'axios';
import { useDisclosure } from '@chakra-ui/react';
import { APIKey, APIToken } from '../Api';
import { Box } from '@chakra-ui/react';
const CreateCheckItems = ({ checkListId, onCheckItemCreate }) => {
  const { isOpen, onOpen, onClose } = useDisclosure();
  function createCheckItem(checkItemName) {
    axios.post(`https://api.trello.com/1/checklists/${checkListId}/checkItems?name=${checkItemName}&key=${APIKey}&token=${APIToken}`)
      .then((response) => {
        onCheckItemCreate(response.data);
      })
      .catch((error) => console.log(error))
  }
  return (
    <>
      <Box as='button' onClick={() => { onOpen() }} ml={8} py={2}> + Add Item</Box>
      <InputModal isOpen={isOpen} onClose={onClose} modalType="create-checkItem" onSubmit={createCheckItem}></InputModal>
    </>
  )
}

export default CreateCheckItems