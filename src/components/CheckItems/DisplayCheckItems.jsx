import React from 'react'
import axios from 'axios'
import { Checkbox, Flex } from '@chakra-ui/react';
import { useEffect } from 'react';
import { APIKey, APIToken } from '../Api';
import { Progress } from '@chakra-ui/react'
import DeleteCheckItems from './DeleteCheckItems';

const DisplayCheckItems = ({ checkListId, checkItems, setCheckItems, cardId, progress, calculateProgress }) => {

    useEffect(() => {
        axios.get(`https://api.trello.com/1/checklists/${checkListId}/checkItems?key=${APIKey}&token=${APIToken}`)
            .then((response) => {
                setCheckItems(response.data);
                calculateProgress(response.data);
            })
            .catch((error) => console.log(error))
    }, [])

    function handleDeletedCheckItem(checkItemId) {
        let filteredCheckItems = checkItems.filter((checkItem) => {
            return checkItem.id != checkItemId;
        })
        calculateProgress(filteredCheckItems);
        setCheckItems(filteredCheckItems);
    }

    function toggleCheckItemState(checkItemId, currentState) {
        const newState = currentState === 'complete' ? 'incomplete' : 'complete';
        axios.put(`https://api.trello.com/1/cards/${cardId}/checkList/${checkListId}/checkItem/${checkItemId}?key=${APIKey}&token=${APIToken}&state=${newState}`)
            .then((response) => {
                const updatedCheckItems = checkItems.map((checkItem) => {
                    return checkItem.id == checkItemId ? { ...checkItem, state: newState } : { ...checkItem };
                })
                setCheckItems(updatedCheckItems);
                calculateProgress(updatedCheckItems);
            })
            .catch((error) => console.log(error))
    }

    return (
        <>
            <div>
                <Progress
                    value={progress}
                    colorScheme={progress === 100 ? "green" : "blue"}
                    size="sm"
                    marginTop={2}
                    marginX={2}
                    mb={4}
                />
            </div>
            {checkItems.map((checkItem) => {
                return (
                    <>
                        <Flex justifyContent='space-between' >
                            <Checkbox border='darkgray' ml={7} py={2} isChecked={checkItem.state == 'complete'} onChange={() => { toggleCheckItemState(checkItem.id, checkItem.state) }}>{checkItem.name}</Checkbox>
                            <DeleteCheckItems checkListId={checkListId} checkItemId={checkItem.id} onCheckItemDeleted={handleDeletedCheckItem} />
                        </Flex>
                    </>
                )
            })}
        </>
    )
}

export default DisplayCheckItems