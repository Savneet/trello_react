import React from 'react'
import DisplayCheckItems from '../CheckItems/DisplayCheckItems';
import CreateCheckItems from '../CheckItems/CreateCheckItems';
import { useState } from 'react';

const CheckItemHandler = ({ checkListId, cardId }) => {

  const [checkItems, setCheckItems] = useState([]);
  const [progress, setProgress] = useState(0);

  const calculateProgress = (checkItems) => {
    if (checkItems.length == 0) {
      setProgress(0)
    }
    else {
      const completedItems = checkItems.filter((checkItem) => checkItem.state === "complete");
      const progressValue = (completedItems.length / checkItems.length) * 100; //Calculating percentage: no of completed items/ total items
      setProgress(progressValue);
    }
  };

  function handleCheckItemCreation(newItem) {
    setCheckItems((prevCheckItems) => {
      const updatedCheckItems = [...prevCheckItems, newItem];
      calculateProgress(updatedCheckItems);
      return updatedCheckItems;
    })
  }

  return (
    <>
      <DisplayCheckItems checkListId={checkListId} checkItems={checkItems} setCheckItems={setCheckItems} cardId={cardId} progress={progress} calculateProgress={calculateProgress} />
      <CreateCheckItems checkListId={checkListId} onCheckItemCreate={handleCheckItemCreation} />
    </>
  )
}

export default CheckItemHandler