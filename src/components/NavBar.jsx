import React from 'react';
import { Box, Flex, Heading } from '@chakra-ui/react';
import { useLocation, useNavigate } from 'react-router-dom';
import { ArrowBackIcon } from '@chakra-ui/icons';

const NavBar = () => {
  const location = useLocation();
  const isHomePage = location.pathname === '/boards'
  const navigate = useNavigate();
  return (
    <>
      <Flex
        justifyContent="space-between"
        py={2}
        bg="gray.50"
        alignItems="center"
      >
        {!isHomePage &&
          <ArrowBackIcon boxSize={5} marginLeft={10} onClick={() => { navigate(-1) }} cursor="pointer" />
        }
        <Heading size="xl" mx="auto" fontWeight="bold">
          Trello
        </Heading>

      </Flex>
      <Box border="1px solid" borderColor="gray.200" />
    </>
  );
};

export default NavBar;