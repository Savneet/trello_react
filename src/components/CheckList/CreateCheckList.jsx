import { Button } from '@chakra-ui/react'
import React from 'react'
import { APIKey, APIToken } from '../Api'
import axios from 'axios'
import { useDisclosure } from '@chakra-ui/react'
import InputModal from '../InputModal'

const CreateCheckList = ({ cardId, onCreateCheckList }) => {
  const { isOpen, onOpen, onClose } = useDisclosure();
  function createCheckList(checkListName) {
    axios.post(`https://api.trello.com/1/cards/${cardId}/checklists?name=${checkListName}&key=${APIKey}&token=${APIToken}`)
      .then((response) => onCreateCheckList(response.data))
      .catch((error) => console.log(error))
  }
  return (
    <>
      <Button onClick={() => { onOpen() }} p={2} marginX={130} >Create CheckList</Button>
      <InputModal isOpen={isOpen} onClose={onClose} modalType="create-checkList" onSubmit={createCheckList}></InputModal>
    </>
  )
}

export default CreateCheckList