import axios from 'axios'
import React, { useEffect } from 'react'
import { APIKey, APIToken } from '../Api';
import { CheckCircleIcon } from '@chakra-ui/icons';
import { Box, Flex } from '@chakra-ui/react';
import DeleteCheckList from './DeleteCheckList';
import CheckItemHandler from '../CheckItems/CheckItemHandler';

const DisplayCheckList = ({ cardId, checkList, setCheckList }) => {

    useEffect(() => {
        axios.get(`https://api.trello.com/1/cards/${cardId}/checklists?key=${APIKey}&token=${APIToken}`)
            .then((response) => {
                setCheckList(response.data);
            })
            .catch((error)=>console.log(error));
    }, [])

    function handleDeletedCheckList(deletedCheckListId) {
        let filteredCheckList = checkList.filter((list) => list.id != deletedCheckListId);
        setCheckList(filteredCheckList);
    }

    return (
        <>
            {
                checkList.map((list) => {
                    return (
                        <>
                            <Box py={2} m={3}>
                                <Flex px={4} py={2} justifyContent="space-between">
                                    <Flex>
                                        <CheckCircleIcon mt={1} />
                                        <Box ml={2} fontWeight='bold'>{list.name}</Box>
                                    </Flex>
                                    <DeleteCheckList checkListId={list.id} onDeletedCheckList={handleDeletedCheckList} />
                                </Flex>
                                <CheckItemHandler checkListId={list.id} cardId={list.idCard} />
                            </Box>
                        </>
                    );
                })
            }
        </>
    )
}

export default DisplayCheckList