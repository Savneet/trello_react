import React from 'react'
import axios from 'axios'
import { APIKey, APIToken } from '../Api'
import { Button } from '@chakra-ui/react'

const DeleteCheckList = ({ checkListId, onDeletedCheckList }) => {

    function deleteCheckList() {
        axios.delete(`https://api.trello.com/1/checklists/${checkListId}?key=${APIKey}&token=${APIToken}`)
            .then((response) => {
                onDeletedCheckList(checkListId)
            })
            .catch((error) => console.log(error))
    }

    return (
        <Button onClick={deleteCheckList} bgColor='#dce6ef'>Delete</Button>
    )
}

export default DeleteCheckList