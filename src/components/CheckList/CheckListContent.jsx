import React from 'react'
import { ModalHeader, ModalBody, ModalContent, ModalCloseButton } from '@chakra-ui/react'
import CreateCheckList from './CreateCheckList'
import DisplayCheckList from './DisplayCheckList'
import { useState } from 'react'

const CheckListContent = ({ cardId, cardName }) => {
    const [checkList, setCheckList] = useState([]);
    function handleCheckListCreation(newList) {
        setCheckList((prevCheckList) => [...prevCheckList, newList])
    }

    return (
        <>
            <ModalContent>
                <ModalHeader>
                    {cardName}
                </ModalHeader>
                <ModalCloseButton />
                <ModalBody>
                    <CreateCheckList cardId={cardId} onCreateCheckList={handleCheckListCreation} />
                    <DisplayCheckList cardId={cardId} checkList={checkList} setCheckList={setCheckList} />
                </ModalBody>
            </ModalContent>
        </>
    )
}

export default CheckListContent