import React, { useState } from 'react';
import {
  Modal,
  ModalOverlay,
  ModalContent,
  ModalHeader,
  ModalCloseButton,
  ModalBody,
  ModalFooter,
  Button,
  FormControl,
  FormLabel,
  Input
} from '@chakra-ui/react';

const InputModal = ({ isOpen, onClose, modalType, onSubmit }) => {
  const initialRef = React.useRef(null);
  const finalRef = React.useRef(null);
  const [inputName, setInputName] = useState('');

  const modalTitle = {
    'create-board': 'Create a new Board',
    'create-list': 'Create a new List',
    'create-card': 'Create a new Card',
    'create-checkList': 'Create a new CheckList',
    'create-checkItem': 'Create a new CheckItem'
  };

  const modalInputLabel = {
    'create-board': 'Board Name',
    'create-list': 'List Name',
    'create-card': 'Card Name',
    'create-checkList': 'CheckList Name',
    'create-checkItem': 'CheckItem Name'
  };

  const handleSubmit = () => {
    onSubmit(inputName);
    setInputName('');
    onClose();
  }

  return (
    <Modal
      initialFocusRef={initialRef}
      finalFocusRef={finalRef}
      isOpen={isOpen}
      onClose={onClose}
    >
      <ModalOverlay />
      <ModalContent>
        <ModalHeader>{modalTitle[modalType]}</ModalHeader>
        <ModalCloseButton />
        <ModalBody pb={6}>
          <FormControl>
            <FormLabel>{modalInputLabel[modalType]}</FormLabel>
            <Input ref={initialRef} placeholder={modalInputLabel[modalType]} onChange={(e) => setInputName(e.target.value)} />
          </FormControl>
        </ModalBody>
        <ModalFooter>
          <Button colorScheme="blue" mr={3} onClick={handleSubmit}>
            Create
          </Button>
          <Button onClick={onClose}>Cancel</Button>
        </ModalFooter>
      </ModalContent>
    </Modal>
  );
};

export default InputModal;