import React, { useEffect } from 'react'
import axios from 'axios';
import { Flex, Modal, ModalOverlay, useDisclosure } from '@chakra-ui/react';
import { useState } from 'react';
import CreateCard from "./CreateCard";
import DeleteCards from './DeleteCards';
import CheckListContent from '../CheckList/CheckListContent'
import { APIKey, APIToken } from '../Api';

const Cards = ({ listId }) => {
    const { isOpen, onOpen, onClose } = useDisclosure()
    const [cards, setCards] = useState([]);
    const [selectedCard, setSelectedCard] = useState(null);
    useEffect(() => {
        axios.get(`https://api.trello.com/1/lists/${listId}/cards?key=${APIKey}&token=${APIToken}`)
            .then((response) => {
                setCards(response.data);
            })
            .catch((error) => console.log(error));
    }, [])

    function handleCreateCard(newCard) {
        setCards((prevCards) => [...prevCards, newCard])
    }

    function handleDeleteCard(cardId, event) {
        onClose();
        let filteredCards = cards.filter((card) => card.id != cardId)
        setCards(filteredCards);
        if (selectedCard && selectedCard.id === response.data.id) {
            setSelectedCard(null);
        }
    }


    function handleCardClick(card) {
        setSelectedCard(card);
        onOpen();
    }

    function handleCloseModal() {
        onClose();
        setSelectedCard(null);
    }

    return (
        <>
            {cards.map((card) => {
                return (
                    <>
                        <Flex key={card.id} bgColor='white' my={3} borderRadius={5} p={3} justifyContent='space-between' onClick={() => { handleCardClick(card) }} cursor='pointer'>
                            {card.name}
                            <DeleteCards cardId={card.id} onCardDelete={() => { handleDeleteCard(card.id) }} onClose={onClose}></DeleteCards>
                        </Flex>
                    </>
                )
            })}

            {selectedCard && (
                <Modal isOpen={isOpen} onClose={handleCloseModal}>
                    <ModalOverlay />
                    <CheckListContent cardId={selectedCard.id} cardName={selectedCard.name} />
                </Modal>
            )}

            <CreateCard listId={listId} onCardCreate={handleCreateCard}></CreateCard>
        </>
    )
}

export default Cards