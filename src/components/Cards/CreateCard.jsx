import React from 'react'
import axios from 'axios'
import InputModal from '../InputModal';
import { useDisclosure } from '@chakra-ui/react';
import { Box } from '@chakra-ui/react';
import { APIKey, APIToken } from '../Api';

const CreateCard = ({ listId, onCardCreate }) => {
  const { isOpen, onOpen, onClose } = useDisclosure();
  function createCard(cardName) {
    axios.post(`https://api.trello.com/1/cards?idList=${listId}&name=${cardName}&key=${APIKey}&token=${APIToken}`)
      .then((response) => {
        onCardCreate(response.data);
      })
      .catch((error) => console.log(error))
  }

  return (
    <>
      <Box as='button' borderRadius={5} p={3} onClick={() => { onOpen() }}> + Add a card </Box>
      <InputModal isOpen={isOpen} onClose={onClose} modalType="create-card" onSubmit={createCard}></InputModal>
    </>
  )
}

export default CreateCard