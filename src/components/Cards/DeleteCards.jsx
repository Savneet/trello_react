import React from 'react'
import axios from 'axios';
import { APIKey, APIToken } from '../Api';
import { CloseIcon } from '@chakra-ui/icons';

const DeleteCards = ({ cardId, onCardDelete, onClose }) => {

  function deleteCard(cardId) {
    axios.delete(`https://api.trello.com/1/cards/${cardId}?key=${APIKey}&token=${APIToken}`)
      .then((response) => {
        onClose();
        onCardDelete();
      }
      )
      .catch((error) => console.log(error));
  }
  return (
    <CloseIcon onClick={(event) => {
      event.stopPropagation();
      deleteCard(cardId)
    }
    } cursor="pointer" mt={1} />
  )
}

export default DeleteCards