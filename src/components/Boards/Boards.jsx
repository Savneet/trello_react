import React, { useEffect, useState } from 'react'
import NavBar from '../NavBar'
import axios from 'axios'
import { Box, Text } from '@chakra-ui/react';
import { useNavigate } from 'react-router-dom';
import { SimpleGrid } from '@chakra-ui/react';
import CreateBoard from './CreateBoard';
import { APIKey, APIToken } from '../Api';

const Boards = () => {
  const navigate = useNavigate();
  const [boards, setBoards] = useState([]);

  useEffect(() => {
    axios.get(`https://api.trello.com/1/members/me/boards?key=${APIKey}&token=${APIToken}`)
      .then((response) => {
        setBoards(response.data);
      })
      .catch((error) => console.error(error))
  }, [])

  function handleCreateBoard(newBoard) {
    setBoards((prevBoards) => [...prevBoards, newBoard]);
  }

  return (
    <>
      <NavBar />
      <Text mt={10} ml={16} fontWeight='bold' fontSize='x-large'>Boards</Text>
      <SimpleGrid templateColumns={{ base: '1fr', md: "repeat(3,1fr)", lg: "repeat(4,1fr)" }} gap={5} mx={16} my={5}>
        <CreateBoard onCreateBoard={handleCreateBoard}></CreateBoard>
        {boards.map((board) => (
          <Box bgColor='#0369A1' textColor="white" height={20} fontWeight="bold" pl={5} pt={3} borderRadius={4} fontSize='large' cursor='pointer' onClick={() => { navigate(`/boards/${board.id}/${board.name}`) }}>
            {board.name}
          </Box>
        ))}
      </SimpleGrid>
    </>
  )
}

export default Boards