import React from 'react'
import axios from 'axios';
import InputModal from '../InputModal';
import { useDisclosure } from '@chakra-ui/react';
import { Box } from '@chakra-ui/react';
import { APIKey, APIToken } from '../Api';

const CreateBoard = ({ onCreateBoard }) => {
  const { isOpen, onOpen, onClose } = useDisclosure();
  function createBoard(boardName) {
    axios.post(`https://api.trello.com/1/boards/?name=${boardName}&key=${APIKey}&token=${APIToken}`)
      .then((response) => {
        onCreateBoard(response.data);
      })
      .catch((error) => console.log(error));
  }

  return (
    <>
      <Box as="button" height={20} bgColor="#E5E7EB" onClick={() => { onOpen('create-board') }}> Create new board </Box>
      <InputModal isOpen={isOpen} onClose={onClose} modalType="create-board" onSubmit={createBoard}></InputModal>
    </>
  )
}

export default CreateBoard