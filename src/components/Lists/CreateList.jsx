import React from 'react'
import InputModal from '../InputModal';
import { useDisclosure } from '@chakra-ui/react';
import { Box } from '@chakra-ui/react';
import axios from 'axios';
import { APIKey, APIToken } from '../Api';

const CreateList = ({ boardId, onListCreated }) => {
  const { isOpen, onOpen, onClose } = useDisclosure();

  const createList = (listName) => {
    console.log(listName);
    axios.post(`https://api.trello.com/1/lists?name=${listName}&idBoard=${boardId}&key=${APIKey}&token=${APIToken}`)
      .then((response) => {
        onListCreated(response.data);
      })
      .catch((error) => console.log(error));
  }

  return (
    <>
      <Box as="button" bgColor="#CBD5E1" borderRadius={3} fontWeight="bold" height={20} p={5} onClick={() => { onOpen() }}>+ Add another list</Box>
      <InputModal isOpen={isOpen} onClose={onClose} modalType="create-list" onSubmit={createList}></InputModal>
    </>
  )
}

export default CreateList