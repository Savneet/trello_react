import React, { useEffect } from 'react'
import { useParams } from 'react-router-dom'
import axios from 'axios';
import { useState } from 'react';
import NavBar from '../NavBar';
import { Box, Flex, SimpleGrid } from '@chakra-ui/react';
import Cards from '../Cards/Cards';
import CreateList from './CreateList';
import DeleteList from './DeleteLists';
import { APIKey, APIToken } from '../Api';

const Lists = () => {
  const params = useParams();
  const boardId = params.id;
  const boardName = params.name;
  const [lists, setLists] = useState([]);

  useEffect(() => {
    axios.get(`https://api.trello.com/1/boards/${boardId}/lists?key=${APIKey}&token=${APIToken}`)
      .then((response) => {
        setLists(response.data);
      })
      .catch((error) => console.log(error))
  }, [])


  const handleListCreated = (newList) => {
    setLists((prevLists) => [...prevLists, newList]);
  };

  const handleDeletedList = (listId) => {
    const deletedList = lists.filter((list) => list.id != listId);
    setLists(deletedList);
  }

  return (
    <>
      <NavBar />
      <Box margin={10} fontSize="x-large" fontWeight="bold" ml={16}>{boardName}</Box>
      <SimpleGrid templateColumns={{ base: '1fr', md: "repeat(3,1fr)", lg: "repeat(4,1fr)" }} gap={8} mx={16} my={5}>
        <CreateList boardId={boardId} onListCreated={handleListCreated}></CreateList>
        {
          lists.map((list) => {
            return (
              <>
                <Box bgColor="#CBD5E1" borderRadius={4} fontWeight="bold" px={5} py={2} key={list.id}>
                  <Flex justifyContent="space-between">
                    {list.name}
                    <DeleteList listId={list.id} onListDelete={handleDeletedList}></DeleteList>
                  </Flex>
                  <Cards listId={list.id}></Cards>
                </Box>
              </>
            )
          })
        }
      </SimpleGrid>
    </>
  )
}

export default Lists