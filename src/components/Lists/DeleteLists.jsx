import React from 'react'
import axios from 'axios';
import { CloseIcon } from '@chakra-ui/icons';
import { APIKey, APIToken } from '../Api';
const DeleteList = ({ listId, onListDelete }) => {
  function deleteList() {
    axios.put(`https://api.trello.com/1/lists/${listId}/closed?value=true&key=${APIKey}&token=${APIToken}`)
      .then(response => {
        onListDelete(response.data.id);
      })
      .catch((error) => {
        console.log(error);
      })
  }

  return (
    <>
      <CloseIcon onClick={() => { deleteList() }} cursor="pointer" mt={1} />
    </>
  )
}

export default DeleteList