import { Routes } from 'react-router-dom'
import { Route } from 'react-router-dom'
import './App.css'
import Boards from './components/Boards/Boards'
import Lists from './components/Lists/Lists'

function App() {

  return (
    <>
    <Routes>
      <Route path='/' element={<Boards/>}></Route>  
      <Route path='/boards/:id/:name' element={<Lists/>}></Route>
    </Routes>
    </>
  )
}

export default App
